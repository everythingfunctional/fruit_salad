require 'pathname'

def findSourceFiles(directories, extensions)
    sources = []
    extensions.each do |extension|
        directories.each do |directory|
            dir = Pathname(directory).cleanpath.to_s
            Dir::glob(File.join("#{dir}", "**", "*#{extension}")).each do |filename|
                sources.push(Pathname(filename).cleanpath.to_s)
            end
        end
    end
    return sources
end

def scanSourceFiles(files, previously_defined_modules)
    sources = Set.new
    modules_contained = {}
    modules_used = Set.new
    programs = Set.new
    files.each do |file|
        new_modules_contained = Set.new
        new_modules_used = Set.new
        new_program = nil
        open(file, 'r') do |f|
            f.each_line do |line|
                if line =~ /(?:^|\r|\n)\s*use +(\w+)\b?/i
                    new_modules_used.add($1.downcase)
                end
                if line =~ /(?:^|\r|\n)\s*module +(\w+)\b?/i
                    module_name = $1.downcase
                    if module_name !~ /procedure/i
                        if previously_defined_modules.include?(module_name)
                            puts "*** Error: module #{module_name} is defined in #{file} and #{previously_defined_modules[module_name].source.file_name}"
                            raise
                        end
                        if modules_contained.include?(module_name)
                            puts "*** Error: module #{module_name} is defined in #{file} and #{modules_contained[module_name].source.file_name}"
                            raise
                        end
                        if new_modules_contained.include?(module_name)
                            puts "*** Error: module #{module_name} is defined more than once in #{file}"
                            raise
                        end
                        new_modules_contained.add(module_name)
                    end
                    if new_program
                        puts "*** Warn: module #{module_name} defined in #{file} is not available for use in other modules or procedures"
                    end
                end
                if line =~ /(?:^|\r|\n)\s*program +(\w+)\b?/i
                    if new_program
                        puts "*** Error: multiple programs in #{file}. #{new_program} and #{$1.downcase}"
                        raise
                    end
                    new_program = $1.downcase
                end
            end
        end
        new_source = SourceFile.new(file, new_modules_contained, new_modules_used - new_modules_contained)
        sources.add(new_source)
        if new_program
            if not new_modules_contained.empty?
                puts "*** Warn: modules (#{new_modules_contained.to_a}) defined in #{file} are not available for use in other modules or procedures"
            end
            programs.add(Program.new(new_program, new_source))
        else
            new_modules_contained.each do |new_module_name|
                modules_contained[new_module_name] = Module.new(new_module_name, new_source)
            end
        end
        modules_used.merge(new_modules_used)
    end
    return sources, modules_contained, modules_used, programs
end

class SourceFile
    attr_reader :file_name, :modules_used, :modules_contained

    def initialize(file_name, modules_contained, modules_used)
        @file_name = file_name
        @modules_contained = modules_contained
        @modules_used = modules_used
    end

    def object(build_dir)
        File.join(build_dir, Pathname(@file_name).basename.sub_ext('.o').to_s)
    end

    def coverageSpec(build_dir)
        File.join(build_dir, Pathname(@file_name).basename.sub_ext('.gcno').to_s)
    end

    def coverageData(build_dir)
        File.join(build_dir, Pathname(@file_name).basename.sub_ext('.gcda').to_s)
    end

    def coverageOutput(build_dir)
        File.join(build_dir, Pathname(@file_name).basename.to_s + ".gcov")
    end

    def ==(other)
        other.file_name == @file_name
    end

    alias eql? ==

    def hash
        @file_name.hash
    end

end

class Module
    attr_reader :name, :source

    def initialize(name, source)
        @name = name
        @source = source
    end

    def ==(other)
        (other.name == @name) && (other.source == @source)
    end

    alias eql? ==

    def hash
        [@name, @source].hash
    end

end

class Program
    attr_reader :name, :source

    def initialize(name, source)
        @name = name
        @source = source
    end

    def ==(other)
        (other.name == @name) && (other.source == @source)
    end

    alias eql? ==

    def hash
        [@name, @source].hash
    end

end

class ModuleSearchTreeNode
    attr_reader :directory, :modules, :rest

    def initialize(directory, modules, rest)
        @directory = directory # where object files are stored
        @modules = modules # the modules at this node
        @rest = rest # A set of additional search trees
    end

    def ==(other)
        (other.directory == @directory) && (other.modules == @modules) && (other.rest == @rest)
    end

    alias eql? ==

    def hash
        [@directory, @modules, @rest].hash
    end

end

def recursiveDependencySearch(params)
    @dependency_search_cache ||= Hash.new do |h, params|
        source = params[:source]
        module_search_tree = params[:module_search_tree]
        needed_objects = Set.new([source.object(module_search_tree.directory)])
        source.modules_used.each do |module_name|
            additional_needed = recursiveModuleSearch(module_name: module_name, module_search_tree: module_search_tree)
            needed_objects.merge(additional_needed)
        end
        h[params] = needed_objects
    end
    return @dependency_search_cache[params]
end

def recursiveModuleSearch(params)
    @module_search_cache ||= Hash.new do |h, params|
        module_name = params[:module_name]
        module_search_tree = params[:module_search_tree]
        if module_search_tree.modules.include?(module_name)
            additional_needed = recursiveDependencySearch(source: module_search_tree.modules[module_name].source, module_search_tree: module_search_tree)
        else
            additional_needed = Set.new
            module_search_tree.rest.each do |sub_tree|
                additional_needed.merge(recursiveModuleSearch(module_name: module_name, module_search_tree: sub_tree))
            end
        end
        h[params] = additional_needed
    end
    return @module_search_cache[params]
end

def usedInTest(source, modules_used_in_tests)
    source.modules_contained.each do |module_name|
        if modules_used_in_tests.include?(module_name)
            return true
        end
    end
    return false
end

def containsTestedCode(program, modules_used_in_tests, available_modules)
    modules_used = followModules(program.source, available_modules)
    modules_used.each do |module_name|
        if modules_used_in_tests.include?(module_name)
            return true
        end
    end
    return false
end

def followModules(source, available_modules)
    used_modules = source.modules_used
    source.modules_used.each do |module_name|
        if available_modules.include?(module_name)
            additional_used = followModules(available_modules[module_name].source, available_modules)
            used_modules.merge(additional_used)
        end
    end
    return used_modules
end

def usesProductionCode(source, available_modules)
    source.modules_used.each do |module_name|
        if available_modules.include?(module_name)
            return true
        end
    end
    return false
end
